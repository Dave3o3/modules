**Click to jump to the [english version](#mirros-modules)**


# mirr.OS Module
Hier gibt es die Core-Module für das [glancr mirr.OS](https://glancr.de/mirr-os/). Im Ordner `dummy` befindet sich ein Beispielmodul, alle wesentlichen Funktionen sind in Kommentaren erklärt.

**Für Github-Gewohnte: Den Code findet ihr im Tab „Repository“**


## Struktur eines glancr-Moduls

    mymodule
    -- assets/
    ---- icon.svg
    -- backend/
    ---- template.php
    ---- styles.css
    ---- scripts.js
    -- frontend/
    ---- template.php
    ---- styles.css
    ---- scripts.js
    -- locale/
    ---- [ISO-3166 lang code, z.B. de_DE]
    ------ LC_MESSAGES
    -------- mymodule.mo
    -------- mymodule.po
    -- info.json


## Aufbau der info.json

### Hinweise
Wir verwenden [Semantic Versioning](http://semver.org). Die Versionsnummer muss also im Format MAJOR.MINOR.PATCH angegeben sein, damit automatische Updates funktionieren.
Die `module_url` und `download_url` findest du auf der Beschreibungsseite deines Moduls auf https://glancr.de/module. Der Platzhalter `__dev-username__` entspricht deinem Usernamen auf glancr.de.

### Beispiel
```json
{
    "module": {
        "name": "your-module-name",
        "version": "1.0.0",
        "description": "Die Modulbeschreibung",
        "module_url": "https://www.glancr.de/profiles/__dev-username__/__modulname__",
        "download_url": "https://www.glancr.de/profiles/__dev-username__/__modulname__/__modulname__.zip",
        "category": "Kategorie (vgl. glancr-Website)",
        "last_update": "yyyy-mm-dd"
    },
    "creator": {
        "name": "Mein Name",
        "contact": "myname@example.com",
        "website": "https://www.my-website.com",
        "profile": "https://www.glancr.de/profiles/__dev-username__"
    }
}
```

## API-Funktionen
* `setConfigValue($key, $value)`: [returns void] Schreibt den angegebenen Wert für $key in die Config-Datenbank.
* `getConfigValue($key)`: [returns configValue] Liest den Wert für $key aus der Config-Datenbank.
* `empty($configValue)`: [returns boolean] Überprüft, ob `getConfigValue($key)` einen undefinierten Wert zurückgibt, z.B. `if(empty(getConfigValue($key)))`.

/*****************************/ ENGLISH VERSION /*****************************/

# mirr.OS modules
Here are the core-modules for the [glancr mirr.OS](https://glancr.de/mirr-os/). In the `dummy` folder there is a sample module where all essential functions are explained in comments.

**For Github-users: the code can be found in the tab „Repository“**

## Structure of a glancr-module

    mymodule
    -- assets/
    ---- icon.svg
    -- backend/
    ---- template.php
    ---- styles.css
    ---- scripts.js
    -- frontend/
    ---- template.php
    ---- styles.css
    ---- scripts.js
    -- locale/
    ---- [ISO-3166 lang code, z.B. de_DE]
    ------ LC_MESSAGES
    -------- mymodule.mo
    -------- mymodule.po
    -- info.json


## Structure of the info.json

### Some hints
We use [Semantic Versioning](http://semver.org). The version number must therefore be specified in the format MAJOR.MINOR.PATCH so that automatic updates will work. The module_url and download_url can be found on the description page of your module at https://glancr.de/module. 
The placeholder __dev-username__ matches your username on glancr.de.

### Example
```json
{
    "module": {
        "name": "your-module-name",
        "version": "1.0.0",
        "description": "Die Modulbeschreibung",
        "module_url": "https://www.glancr.de/profiles/__dev-username__/__modulname__",
        "download_url": "https://www.glancr.de/profiles/__dev-username__/__modulname__/__modulname__.zip",
        "category": "Kategorie (vgl. glancr-Website)",
        "last_update": "yyyy-mm-dd"
    },
    "creator": {
        "name": "Mein Name",
        "contact": "myname@example.com",
        "website": "https://www.my-website.com",
        "profile": "https://www.glancr.de/profiles/__dev-username__"
    }
}
```

## API functions
* `setConfigValue($key, $value)`: [returns void] writes the specified $key value to the config-database.
* `getConfigValue($key)`: [returns configValue] reads the $key value from the config-database.
* `empty($configValue)`: [returns boolean] checks if `getConfigValue($key)` returns an undefined value, e.g. `if(empty(getConfigValue($key)))`.
